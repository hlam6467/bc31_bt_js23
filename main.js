/**
 * Bài tập 1
 * Input
 * tiền ngày lương  100000
 * Các bước xử lý
 * Nhập số ngày làm *100000
 * Output
 * tổng lương = null
 */
function tienLuong() {
  var ngayLam = document.getElementById("ngayCong").value;
  var tongLuong = ngayLam * 100000;
  document.getElementById(
    "tien-luong"
  ).innerHTML = `<div> Tổng tiền lương là : ${tongLuong}</div>`;
}
// bài tập 2
/**
 * input
 * nhập 5 số thực
 * cách sử lý
 * cộng 5 số thực với nhau r chia cho 5 số thực
 * output
 * trung bình cộng =?
 */
function trungBinh() {
  var numberOne = document.getElementById("soDau").value * 1;
  var numberTwo = document.getElementById("soHai").value * 1;
  var numberThree = document.getElementById("soBa").value * 1;
  var numberFour = document.getElementById("soBon").value * 1;
  var numberFive = document.getElementById("soNam").value * 1;
  var trungBinhCong =
    (numberOne + numberTwo + numberThree + numberFour + numberFive) / 5;
  document.getElementById(
    "trung-binh"
  ).innerHTML = `<div>Kết Quả Là : ${trungBinhCong}</div>`;
}
// bài tập 3
/**input
 * giaUsd=23500
 * cách xử lý
 * lấy giaUsd nhân với số tiền đô muốn đổi sang VND
 * output
 * giaVND=?
 */
function tinhTien() {
  var giaTienValue = document.getElementById("giaTien").value * 1;
  var tienVnd = giaTienValue * 23500;
  console.log("tienVnd", tienVnd);
  document.getElementById("quy-doi").innerHTML =
    " <div>Số tiền VND : " + tienVnd + "</div> ";
}
// bài tập 4
/**input
 * chiều dài
 * chiều rộng
 * cách xử lý
 * diệntích= dài*rộng
 * chuvi=(dài + rộng) *2
 * output
 * Diện tích= ?
 * Chuvi= ?
 */
function dienTich() {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;
  var dienTich = chieuDai * chieuRong;
  console.log("dienTich", dienTich);
  document.getElementById(
    "tinh-dientich"
  ).innerHTML = `<div> Diện tích HCN là: ${dienTich}</div>`;
}
function chuVi() {
  var chieuDai = document.getElementById("chieuDai").value * 1;
  var chieuRong = document.getElementById("chieuRong").value * 1;
  var chuVi = (chieuDai + chieuRong) * 2;
  console.log("chuVi", chuVi);
  document.getElementById(
    "tinh-chuvi"
  ).innerHTML = `<div> Chu vi HCN là: ${chuVi}</div>`;
}
// bài tập 5
/**
 * input
 * 1 số có 2 chữ số
 * cách xử lý
 * vì kết quả ra là chuỗi string nên có thể lấy từng phẩn tử trong chuỗi để cộng lại với nhau
 * output
 *  tổng của 2 chữ số trong 1 số có 2 chũ số
 */
function tongSo() {
  var haiChuSo = document.getElementById("soHaiChuSo").value;
  console.log("haiChuSo", haiChuSo);
  var soDau = haiChuSo.slice(0, 1) * 1;
  var soHai = haiChuSo.slice(1) * 1;
  var tongHaiChuSo = (soDau + soHai) * 1;
  console.log("tongHaiChuSo", tongHaiChuSo);
  document.getElementById("tong-la").innerHTML = `
  <div>Tổng là : ${tongHaiChuSo}</div>`;
}
